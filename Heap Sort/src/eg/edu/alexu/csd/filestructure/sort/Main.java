package eg.edu.alexu.csd.filestructure.sort;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println();
		IHeap heap = new Heap();
        heap.insert(1);
        heap.insert(2);
        heap.insert(10);
        heap.insert(5);
        heap.insert(6);
        heap.insert(11);
        
        System.out.println();
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        System.out.println(heap.extract());
        

	}

}
