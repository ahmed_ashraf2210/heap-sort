package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Collection;

public class Heap<T extends Comparable<T>>implements IHeap<T>  {
	private int size;
	ArrayList<INode> heap = new ArrayList<>();

	@SuppressWarnings({ "unused", "hiding" })
	private class Node<T extends Comparable<T>> implements INode<T> {
		private int index;
		private T value;
		
		public Node(int index) {
			this.index = index;
		}
		@SuppressWarnings("unchecked")
		@Override
		public INode<T> getLeftChild() {
			// TODO Auto-generated method stub
			if(2 * index >= heap.size()) {
				return null;
			}
			return  heap.get(2 * index - 1);
		}

		@SuppressWarnings("unchecked")
		@Override
		public INode<T> getRightChild() {
			// TODO Auto-generated method stub
			if(2 * index + 1  >= heap.size()) {
				return null;
			}
			return heap.get(2 * index);
		}

		@SuppressWarnings("unchecked")
		@Override
		public INode<T> getParent() {
			// TODO Auto-generated method stub
			if(index == 1) {
				return null;
			}
			return heap.get((index / 2)-1);
		}

		@Override
		public T getValue() {
			// TODO Auto-generated method stub
			return value;
		}

		@Override
		public void setValue(T value) {
			// TODO Auto-generated method stub
			this.value = value;
			
		}
		
		
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public INode<T> getRoot() {
		// TODO Auto-generated method stub
		if(heap.size() == 0) {
			return null;
		}
		return heap.get(0);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void heapify(INode<T> node) {
		// TODO Auto-generated method stub
		if(node != null) {
			INode<T> leftChild = node.getLeftChild();
			INode<T> rightChild = node.getRightChild();
			INode<T> largest = node;
			if (leftChild != null && leftChild.getValue().compareTo(node.getValue()) == 1) {
				largest = leftChild;
			}
			if(rightChild != null && rightChild.getValue().compareTo(leftChild.getValue()) == 1 && rightChild.getValue().compareTo(node.getValue()) == 1) {
				largest = rightChild;
			}
			if(!largest.getValue().equals(node.getValue())) {
				swap(node, largest);
				heapify(largest);
			}
		}
		
	}
	void swap(INode<T> n, INode<T> l) {
		T temp = n.getValue();
		n.setValue(l.getValue());
		l.setValue(temp);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public T extract() {
		// TODO Auto-generated method stub
		if(size == 0) return null;
		swap(getRoot(), heap.get(size-1));
		size--;
		heapify(getRoot());
		return (T) heap.get(size).getValue();
	}

	@Override
	public void insert(T element) {
		// TODO Auto-generated method stub
		INode<T> node = new Node<T>(heap.size() == 0 ? 1 : heap.size() + 1);
		node.setValue(element);
		heap.add(node);
		
		while(node.getParent() != null) {
			if(node.getValue().compareTo(node.getParent().getValue()) > 0) {
				swap(node, node.getParent());
				node = node.getParent();
			}else break;
		}
		size++;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void build(Collection unordered) {
		// TODO Auto-generated method stub
		heap.clear();
		heap.addAll(unordered);
		size = heap.size();
		for(int i = unordered.size() / 2; i >= 0; i--) {
			heapify(heap.get(i));
		}
	}
}
